// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Example clock-tree generator and buffer insertion engine for the LibrEDA framework.
//!
//! * [`SimpleBufferInsertionEngine`]
//! * [`create_clock_trees`]
//!
//! ## Buffer Insertion
//! High fan-out nets (nets with many sinks) cause a high capacitive load and
//! usually require the insertion of additional buffers
//! to assert a certain signal quality (slope and delay).
//!
//! ## Clock Tree
//! Clock signals and reset signals usually drive many storage cells and hence must be buffered
//! with a tree structured buffering scheme. The outputs of such a buffer tree are usually constrained
//! by timing requirements such as the relative arrival times of the signal edges at the outputs.
//!
//! This crate provides simple algorithms to create buffer trees. The algorithms here can be used
//! also for small clock trees but currently there's no timing optimization being done.
//! However, the tree structure is aware of the placement such that the wiring length can be kept small.
//!
//! Note that the task of clock-tree generators and buffer-tree generators is mostly about
//! finding a distribution circuit and locations of the used buffer cells. The actual wiring
//! is best delegated to a signal router. Some implementations might for example take
//! a router as an argument.
//!
//! # References
//!
//! A comprehensive chapter about clock-tree synthesis can be found in "Electronic Design Automation: Synthesis, Verification, and Test" (ISBN 9780123743640)
//!
//! [`SimpleBufferInsertionEngine`]: simple_buffer_tree::SimpleBufferInsertionEngine
//! [`create_clock_trees`]: simple_clock_tree::create_clock_trees

#![deny(missing_docs)]

pub mod clock_tree_specification;
pub mod simple_buffer_tree;
pub mod simple_clock_tree;
