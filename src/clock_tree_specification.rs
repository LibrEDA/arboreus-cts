// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Specification of a clock-tree.

use libreda_pnr::db::{NetlistBase, TerminalId};
use std::collections::HashMap;

/// Specify a clock-tree by the clock source net and the target skews.
/// This is used as input argument for clock-tree generator engines.
pub trait SimpleClockTreeSpecification<N>
where
    N: NetlistBase,
{
    /// Get clock net.
    /// The clock net must be connected to the clock source and to the clock sinks.
    /// The clock source node must be the only driving terminal on the clock net.
    fn clock_net(&self) -> N::NetId;

    /// Get the desired clock skews at the sink nodes.
    /// The sink nodes must all belong to the same net as the clock source.
    fn target_skews(&self) -> HashMap<TerminalId<N>, f64>;
}
