<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Arboreus - Buffer-tree generator and Clock-tree synthesizer (CTS)

Arboreus is a clock-tree and buffer-tree generator for the [LibrEDA framework](https://libreda.org).
It's purpose is to convert high-fanout nets into fan-out trees by inserting buffers.

High fan-out nets (nets with many sinks) usually require the insertion of additional buffers
to assert a certain signal quality (slope and delay).
Clock signals and reset signals usually drive many storage cells and hence must be buffered
with a tree structured buffering scheme. The outputs of such a buffer tree are usually constrained
by timing requirements such as the relative arrival times of the signal edges at the outputs (skew).

This crate provides simple algorithms to create buffer trees. The algorithms here can be used for demonstration purposes
also for small clock trees but currently do not implement timing optimization.

All the algorithms work on the `NetlistEdit` trait defined in the `libreda-db` crate.

## Documentation

The documentation is included in the code and can be viewed with:

```bash
cargo doc --open
```

Alternatively the documentation is also hosted [here](https://libreda.org/doc/arboreus_cts/index.html) but may be not up to date.


## License
This code is licensed under the AGPL-3.0-or-later. See the LICENSES directory for details.
